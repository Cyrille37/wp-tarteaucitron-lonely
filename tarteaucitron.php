<?php
/*
Plugin Name: wp-tarteaucitron-lonely - GDPR/RGPD legislation for external services & cookies
Plugin URI: https://framagit.org/Cyrille37/wp-tarteaucitron-lonely/
Description: Comply with GDPR/RGPD legislation for external services and cookies. It's a fork from tarteaucitron.io Wordpress plugin version 1.6.4.
Version: 1.6.4
Text Domain: tarteaucitronjs
Domain Path: /languages/
Author: tarteaucitron.io, Cyrille37
Author URI: https://framagit.org/Cyrille37/
Licence: GPLv2

*/

defined('ABSPATH') or die('No script kiddies please!');

//do_action('qm/debug', 'TarteAuCitron');

require_once(__DIR__ . '/libraries/action-scheduler/action-scheduler.php');

define('TARTEAUCITRON_FILE', __FILE__);
define('TARTEAUCITRON_PATH', realpath(plugin_dir_path(TARTEAUCITRON_FILE)) . '/');
define('TARTEAUCITRON_URL', plugins_url('wp-tarteaucitron-lonely'));

require(TARTEAUCITRON_PATH . '/Admin.php');
require(TARTEAUCITRON_PATH . '/Sidebars.php');
require(TARTEAUCITRON_PATH . '/Widgets.php');

/**
 * @todo Only youtube thumbnail is done, other service have to be implemented.
 */
class TarteAuCitron
{
    /**
     * Used to identify every plugin stuff like "textdomaine", ...
     */
    const PLUGIN_KEY = 'tarteaucitron-lonely';

    protected $cache_path;
    protected $cache_url;

    public function __construct()
    {
        $this->cache_path = WP_CONTENT_DIR . DIRECTORY_SEPARATOR . self::PLUGIN_KEY;
        $this->cache_url = content_url(self::PLUGIN_KEY);

        add_action('send_headers', [$this, 'send_security_headers']);

        add_action('plugins_loaded', [$this, 'load_textdomain']);
        add_action('wp_enqueue_scripts', [$this, 'tarteaucitron_user_css_js'], 1);
        add_action('wp_head', [$this, 'tarteaucitronForceLocale'], 1);
        add_action('admin_bar_menu', [$this, 'tarteaucitron_toolbar'], PHP_INT_MAX);
        //add_action('admin_print_styles', [$this, '_tarteaucitron_admin_bar_css'], 100);
        add_action('wp_print_styles', [$this, '_tarteaucitron_admin_bar_css'], 100);

        add_filter('embed_oembed_html', [$this, 'filter_embed_oembed_html'], PHP_INT_MAX, 4);
        add_action('tarteaucitronjs_download_thumbnail', [$this, 'action_download_thumbnail']);

        add_action('init', [$this, 'oembed_register_handlers']);

        if (WP_DEBUG) {
            add_filter('https_local_ssl_verify', function () {
                return false;
            });
            add_filter('https_ssl_verify', function () {
                return false;
            });
        }
    }

    /**
     * https://oembed.com/
     */
    public function oembed_register_handlers()
    {
        // Prezi handler
        //
        // Prezi does not have an oEmbed endpoint :-(
        // https://prezi.com/p/mtvm9e7wpfe4
        //wp_oembed_add_provider('#^https://(www.)?prezi.com/p/.*#i', plugins_url('wp-tarteaucitron-lonely/oembed-provider.php'), true);
        //
        wp_embed_register_handler(
            'prezi',
            '#^https://(?:www.)?prezi.com/p/(.*)(?:/.*)?$#iU',
            [$this, 'oembed_prezi_handler'],
        );
    }

    /**
     * Evaluate your score: https://securityheaders.com
     * Hardening your HTTP response headers
     * - https://scotthelme.co.uk/hardening-your-http-response-headers/
     * - https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers
     */
    public function send_security_headers()
    {
        if (headers_sent()) {
            do_action('qm/debug', ['action_send_headers' => 'headers_sent']);
            return;
        }
        //do_action('qm/debug', ['action_send_headers' => 'sending']);

        // Strict-Transport-Security
        // HTTP Strict Transport Security (HSTS)
        // https://scotthelme.co.uk/hsts-the-missing-link-in-tls/
        header('Strict-Transport-Security: max-age=31536000; includeSubDomains');

        // X-XSS-Protection
        header('X-XSS-Protection: "1; mode=block"');

        // X-Content-Type-Options
        // https://scotthelme.co.uk/hardening-your-http-response-headers/#x-content-type-options
        header('X-Content-Type-Options: nosniff');

        // X-Frame-Options (XFO)
        // Sites can use this to avoid click-jacking attacks, by ensuring that their content is not embedded into other sites.
        // https://scotthelme.co.uk/hardening-your-http-response-headers/#x-frame-options
        // https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Frame-Options
        header('X-Frame-Options: DENY');
        header('Content-Security-Policy: frame-ancestors \'self\';');

        // Referrer-Policy
        // https://scotthelme.co.uk/a-new-security-header-referrer-policy/
        // https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Referrer-Policy
        //header('Referrer-Policy: no-referrer');
        header('Referrer-Policy: same-origin');

        // Content-Security-Policy (CSP)
        // https://scotthelme.co.uk/content-security-policy-an-introduction/
        //header('Content-Security-Policy: default-src \'self\'' );
        //header('Content-Security-Policy: default-src \'self\'; script-src-attr '.site_url().'; script-src-elem '.site_url() );

        // Permissions-Policy
        // https://scotthelme.co.uk/goodbye-feature-policy-and-hello-permissions-policy/
    }

    public function load_textdomain()
    {
        load_plugin_textdomain(self::PLUGIN_KEY, false, TARTEAUCITRON_PATH . '/languages');
    }

    function tarteaucitron_user_css_js()
    {
        // @todo : If tarteaucitron.init( useExternalCss=false ) the tarteaucitron.css file will be loaded
        // but "user.css" will be loaded before :( so cannot override rules :(
        wp_register_style('tarteaucitronjs', TARTEAUCITRON_URL . '/css/tarteaucitron.css');
        wp_enqueue_style('tarteaucitronjs');

        wp_register_style('tarteaucitronjs-user', TARTEAUCITRON_URL . '/css/user.css');
        wp_enqueue_style('tarteaucitronjs-user');
    }

    function tarteaucitronForceLocale()
    {
        if (is_admin() || isset($_GET['fl_builder'])) {
            return;
        }

        $domain = $_SERVER['SERVER_NAME'];

        $allowed     = array('ar', 'bg', 'cn', 'cs', 'da', 'de', 'et', 'el', 'en', 'es', 'fi', 'fr', 'hu', 'it', 'ja', 'nl', 'pl', 'pt', 'ro', 'ru', 'se', 'sk', 'tr', 'vi', 'lv', 'uk', 'no', 'ca', 'sv', 'zh');
        $locale     = substr(get_locale(), 0, 2);

        if (in_array($locale, $allowed)) {

            echo '<script>
		var tarteaucitronForceLanguage = "' . $locale . '";
		</script>';
        }

        $loc = "";
        if (in_array($locale, $allowed)) {
            $loc = 'locale=' . $locale . '&';
        }

        // fork Cyrille37
        //echo '<script type="text/javascript" src="https://tarteaucitron.io/load.js?'.$loc.'iswordpress=true&domain='.$domain.'&uuid='.tac_sanitize(get_option('tarteaucitronUUID'), 'uuid').'"></script>';
        // Quel script ?
        //echo '<script type="text/javascript" src="'.plugins_url('wp-tarteaucitron-lonely/js/tarteaucitron-pro.js').'"></script>';
        echo '<script type="text/javascript" src="' . plugins_url('wp-tarteaucitron-lonely/js/tarteaucitron.js') . '"></script>';
        //echo '<script type="text/javascript" src="' . plugins_url('wp-tarteaucitron-lonely/js/tarteaucitron.services.js') . '"></script>';
        echo '<script type="text/javascript" src="' . plugins_url('wp-tarteaucitron-lonely/js/tarteaucitron.init.js') . '"></script>';
    }

    function tarteaucitron_toolbar($wp_admin_bar)
    {
        $wp_admin_bar->add_menu(array(
            'id'    => 'tarteaucitronjs',
            'title' => '<span class="ab-icon"></span> tarteaucitron.js',
            'href'  => admin_url('options-general.php?page=tarteaucitronjs'),
        ));
    }

    function _tarteaucitron_admin_bar_css()
    {
        if (current_user_can('manage_options')) {
            wp_register_style(
                'tarteaucitronjs-admin-bar',
                // fork Cyrille37
                //plugins_url('tarteaucitronjs/css/admin-bar.min.css'),
                plugins_url('wp-tarteaucitron-lonely/css/admin-bar.min.css'),
                array(),
                '1'
            );

            wp_enqueue_style('tarteaucitronjs-admin-bar');
        }
    }

    /**
     * for known wordpress oembed handlers
     */
    function filter_embed_oembed_html($cache, $url, $attr, $post_ID)
    {

        if (WP_DEBUG) {
            do_action('qm/debug', ['url' => $url]);
            error_log(__METHOD__ . ' ' . $url);
        }

        if (is_admin() || isset($_GET['fl_builder'])) {
            return;
        }

        $html = null;

        $url = esc_url($url);
        $url_parse = wp_parse_url($url);
        $host = str_replace(['www.', 'player.'], '', $url_parse['host']);
        // error_log(__METHOD__.' host:'.$host);
        if (in_array($host, array('youtube.com', 'youtube.fr', 'youtu.be'))) {
            $html = $this->process_youtube($url);
        } else if ($host == 'vimeo.com') {
            $html = $this->process_vimeo($url);
        } else if ($host == 'dailymotion.com') {
            $html = $this->process_dailymotion($url);
        }

        return empty($html) ? $cache : $html;
    }

    /**
     * An unknown wordpress oembed handler for Prezi
     */
    public function oembed_prezi_handler($matches, $attr, $url, $rawattr)
    {
        //error_log(__METHOD__ . ' matches:' . print_r($matches, true) . ' attr:' . print_r($attr, true) . ' rawattr:' . print_r($rawattr, true));

        $id = $matches[1];

        // those dimension are stranges :-(
        //$width = isset($attr['width']) ? $attr['width'] : 500;
        //$height = isset($attr['height']) ? $attr['height'] : 281;
        $width = 560;
        $height = 315;

        // At Post edition time "discover==1", no is_admin is_blog_admin flag available.
        if (isset($attr['discover']) && $attr['discover'] == 1) {
            return  '<iframe src="https://prezi.com/p/embed/' . $id
                . '" height="' . $height . '" width="' . $width . '" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" allow="autoplay; fullscreen" ></iframe>';
        }

        $html = $this->process_prezi($url);
        return $html;
    }

    /**
     * Async job for thumbnail download.
     * @param array $args must contains: 'service', 'videoId' & 'file_name'
     */
    function action_download_thumbnail($args)
    {
        if (!isset($args['service']) || !isset($args['file_name']))
            throw new \InvalidArgumentException('"service" argument is mandatory');

        //error_log(__FUNCTION__ . ' args: ' . var_export($args, true));

        if (!is_dir($this->cache_path))
            mkdir($this->cache_path, 0777, true);

        $file_name = $args['file_name'];
        if (file_exists($file_name)) {
            return;
        }

        $url = null;
        switch ($args['service']) {
            case 'youtube':
                //$url = 'https://i.ytimg.com/vi/' . $args['videoId'] . '/maxresdefault.jpg';
                $url = 'https://www.youtube.com/oembed?url=' . urlencode('https://youtube.com/watch?v=' . $args['videoId']) . '&format=json';
                $content = file_get_contents($url);
                $url = json_decode($content);
                $url = $url->thumbnail_url;
                break;
            case 'dailymotion':
                //$url = 'https://www.dailymotion.com/thumbnail/video/' . $args['videoId'];
                $url = 'https://api.dailymotion.com/video/' . $args['videoId'] . '?fields=thumbnail_480_url';
                $content = file_get_contents($url);
                $url = json_decode($content);
                $url = $url->thumbnail_480_url;
                break;
            case 'vimeo':
                $url = 'https://vimeo.com/api/v2/video/' . $args['videoId'] . '.json';
                $content = file_get_contents($url);
                $url = json_decode($content);
                $url = $url[0]->thumbnail_large;
                break;
            case 'prezi':
                $url = 'https://prezi.com/p/' . $args['videoId'];
                $content = file_get_contents($url);
                if (!preg_match('#<meta property="og:image" content="(.*)" />#U', $content, $m))
                    throw new \RuntimeException('Failed to find image for Prezi');
                $url = $m[1];
                break;
        }
        if (!$url)
            return;

        $content = file_get_contents($url);
        file_put_contents(
            $file_name,
            $content
        );
    }

    /**
     * video data: https://vimeo.com/api/v2/video/792716788.json
     */
    protected function process_vimeo($url)
    {
        if (!preg_match('#/([0-9]+)(\?.*)?$#', $url, $m) || !isset($m[1]))
            return null;
        $videoId = $m[1];
        $service = 'vimeo';

        $file_name = $this->cache_path . DIRECTORY_SEPARATOR . $service . '-' . $videoId . '.jpg';
        $bg_img = $this->cache_url . '/' .  $service . '-' . $videoId . '.jpg';

        $class = '';
        $style = '';
        if (!file_exists($file_name)) {
            $job_args = array('service' => $service, 'videoId' => $videoId, 'file_name' => $file_name);
            if (!as_next_scheduled_action('tarteaucitronjs_download_thumbnail', ['job_args' => $job_args], 'tarteaucitron')) {
                do_action('qm/debug', ['where' => 'process_vimeo', 'queuing']);
                as_enqueue_async_action('tarteaucitronjs_download_thumbnail', ['job_args' => $job_args], 'tarteaucitron');
            }
            $style = 'height:50vw';
            $class = '';
        } else {
            //$style = 'height:50vw; background-image:url(\''.$bg_img.'\');' ;
            $style = 'background-image:url(\'' . $bg_img . '\');';
            $class = 'with-thumbnail';
        }

        return '<script>document.addEventListener("DOMContentLoaded", function() {(tarteaucitron.job = tarteaucitron.job || []).push("vimeo");});</script>'
            . '<div class="tarteaucitron_player vimeo_player ' . $class . '" videoID="' . $videoId . '"'
            . ' data-url="' . $url . '"'
            . ' style="' . $style . '"'
            //.' width="100%" height="100%" style="height:50vw"></div>';
            . ' width="100%" height="100%" style="height:50vw"></div>';
    }

    protected function process_prezi($url)
    {
        if (!preg_match('#^https://(?:www.)?prezi.com/p/(.*)(?:/.*)?$#iU', $url, $m) || !isset($m[1]))
            return null;
        $videoId = $m[1];
        $service = 'prezi';

        $file_name = $this->cache_path . DIRECTORY_SEPARATOR . $service . '-' . $videoId . '.png';
        $bg_img = $this->cache_url . '/' .  $service . '-' . $videoId . '.png';

        $class = '';
        $style = '';
        if (!file_exists($file_name)) {
            $job_args = array('service' => $service, 'videoId' => $videoId, 'file_name' => $file_name);
            if (!as_next_scheduled_action('tarteaucitronjs_download_thumbnail', ['job_args' => $job_args], 'tarteaucitron')) {
                do_action('qm/debug', ['where' => __METHOD__, 'queuing']);
                as_enqueue_async_action('tarteaucitronjs_download_thumbnail', ['job_args' => $job_args], 'tarteaucitron');
            }
            $style = 'height:50vw';
            $class = '';
        } else {
            //$style = 'height:50vw; background-image:url(\'' . $bg_img . '\');';
            $style = 'background-image:url(\'' . $bg_img . '\');';
            $class = 'with-thumbnail';
        }

        return '<script>document.addEventListener("DOMContentLoaded", function() {(tarteaucitron.job = tarteaucitron.job || []).push("prezi");});</script>'
            . '<div class="tarteaucitron_player prezi-canvas ' . $class . '" data-id="' . $videoId . '"'
            . ' data-url="' . $url . '"'
            . ' style="' . $style . '"'
            . ' width="100%" height="100%"></div>';
    }

    /**
     * Dailymotion video url: https://www.dailymotion.com/video/x4muspr
     * 
     * Dailymotion API:
     * https://developers.dailymotion.com/api/#video-fields
     * https://api.dailymotion.com/video/x4muspr?fields=thumbnail_720_url
     * response: {"thumbnail_720_url":"https:\/\/s2.dmcdn.net\/v\/Gj5BV1Zl1hNgi1bur\/x720"}
     * https://api.dailymotion.com/video/x4muspr?fields=thumbnail_480_url
     * response: {"thumbnail_480_url":"https:\/\/s1.dmcdn.net\/v\/Gj5BV1Zl1hNom9Ws8\/x480"}
     */
    protected function process_dailymotion($url)
    {
        //do_action('qm/debug', ['where' => 'process_dailymotion']);

        if (!preg_match('#/video/([a-z0-9]+)(\?.*)?$#', $url, $m) || !isset($m[1]))
            return null;
        $videoId = $m[1];
        $service = 'dailymotion';

        $file_name = $this->cache_path . DIRECTORY_SEPARATOR . $service . '-' . $videoId . '.jpg';
        $bg_img = $this->cache_url . '/' .  $service . '-' . $videoId . '.jpg';

        $class = '';
        $style = '';
        if (!file_exists($file_name)) {
            $job_args = array('service' => $service, 'videoId' => $videoId, 'file_name' => $file_name);
            if (!as_next_scheduled_action('tarteaucitronjs_download_thumbnail', ['job_args' => $job_args], 'tarteaucitron')) {
                do_action('qm/debug', ['where' => 'process_dailymotion', 'queuing']);
                as_enqueue_async_action('tarteaucitronjs_download_thumbnail', ['job_args' => $job_args], 'tarteaucitron');
            }
            $style = 'height:50vw';
            $class = '';
        } else {
            //$style = 'height:50vw; background-image:url(\'' . $bg_img . '\');';
            $style = 'background-image:url(\'' . $bg_img . '\');';
            $class = 'with-thumbnail';
        }

        return '<script>document.addEventListener("DOMContentLoaded", function() {(tarteaucitron.job = tarteaucitron.job || []).push("dailymotion");});</script>'
            . '<div class="tarteaucitron_player dailymotion_player ' . $class . '" videoID="' . $videoId . '"'
            . ' data-url="' . $url . '"'
            . ' style="' . $style . '"'
            //. ' width="100%" height="100%" showinfo="1" autoplay="0" ></div>';
            . ' width="500" height="281" showinfo="1" autoplay="0" ></div>';
    }

    /**
     * thumbnail
     * - https://yt-thumb.canbeuseful.com/fr
     */
    protected function process_youtube($url)
    {
        //do_action('qm/debug', ['service' => 'youtube']);

        $youtube_args = null;
        parse_str(parse_url($url, PHP_URL_QUERY), $youtube_args);

        if (!is_array($youtube_args) || empty($youtube_args['v']))
            return null;

        $videoId = $youtube_args['v'];
        $service = 'youtube';

        $file_name = $this->cache_path . DIRECTORY_SEPARATOR . $service . '-' . $videoId . '-maxresdefault.jpg';
        $bg_img = $this->cache_url . '/' .  $service . '-' . $videoId . '-maxresdefault.jpg';

        $style = '' ;
        $class = '';
        if (!file_exists($file_name)) {
            $job_args = array('service' => $service, 'videoId' => $videoId, 'file_name' => $file_name);
            if (!as_next_scheduled_action('tarteaucitronjs_download_thumbnail', ['job_args' => $job_args], 'tarteaucitron')) {
                do_action('qm/debug', ['where' => 'process_youtube', 'queuing']);
                as_enqueue_async_action('tarteaucitronjs_download_thumbnail', ['job_args' => $job_args], 'tarteaucitron');
            }
            //$style = 'height:50vw';
            //$class = '';
        } else {
            //$style = 'height:50vw; background-image:url(\'' . $bg_img . '\'); ';
            $style = 'background-image:url(\'' . $bg_img . '\');';
            $class = 'with-thumbnail';
        }

        return '<script>document.addEventListener("DOMContentLoaded", function() {(tarteaucitron.job = tarteaucitron.job || []).push("youtube");});</script>'
            . '<div class="tarteaucitron_player youtube_player ' . $class . '" videoID="' . $videoId . '"'
            . ' data-url="' . $url . '"'
            . ' style="' . $style . '"'
            //. ' width="100%" height="100%" theme="light" rel="0" controls="1" showinfo="1" autoplay="0"></div>';
            . ' width="100%" height="100%" theme="light" rel="0" controls="1" showinfo="1" autoplay="0"></div>';
    }
}

new TarteAuCitron();
