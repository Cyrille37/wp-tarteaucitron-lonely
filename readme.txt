=== wp-tarteaucitron-lonely - Cookies legislation & GDPR ===

Contributors: amauric, cyrille37
Tags: tarteaucitron, cookie, rgpd, gdpr
Requires at least: 2.8
Tested up to: 6.1
Stable tag: trunk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Comply with the Cookies and GDPR legislation.

== Description ==

This wordpresse plugin is a fork from tarteaucitron.js, the most used script to get in compliance with cookies and GDPR, created by Amauric at https://tarteaucitron.io/.

The fork, from tarteaucitron v1.6.4, allows you to operate without an account on tarteaucitron.io.

== Installation ==

= WordPress Admin Method =

TODO

== Changelog ==

= 1.6.5 =

= 1.6.4 =
* Initial code before the fork

